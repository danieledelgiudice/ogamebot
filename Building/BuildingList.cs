﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace OGame.Building
{
    class BuildingList
    {
        private readonly List<Buildings> _buildingList;

        public BuildingList()
        {
            _buildingList = new List<Buildings>();
        }

        public BuildingList(List<Buildings> buildingList)
        {
            _buildingList = new List<Buildings>(buildingList);
        }

        public void RemoveAt(int index)
        {
            lock (this)
            {
                _buildingList.RemoveAt(index);
            }
        }

        public void Clear()
        {
            lock (this)
            {
                _buildingList.Clear();
            }
        }

        public void Add(Buildings b)
        {
            lock (this)
            {
                _buildingList.Add(b);
            }
        }

        public Buildings this[int i]
        {
            get
            {
                lock (this)
                {
                    return _buildingList[i];
                }
            }

            set
            {
                lock (this)
                {
                    _buildingList[i] = value;
                }
            }
        }

        public int Count
        {
            get { return _buildingList.Count; }
        }

        public void Save(string filename)
        {
            Stream stream = File.Open(filename, FileMode.Create);
            var bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, _buildingList);
            stream.Close();
        }

        public static BuildingList LoadFromFile(string filename)
        {
            Stream stream = File.Open(filename, FileMode.Open);
            var bFormatter = new BinaryFormatter();
            var bl = (BuildingList)bFormatter.Deserialize(stream);
            stream.Close();
            return bl;
        }
    }
}
