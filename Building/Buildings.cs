﻿using System.ComponentModel;
using OGame.Browser;
namespace OGame.Building
{

    /// <summary>
    /// Enum che contiene la lista di tutti gli edifici
    /// </summary>
    enum Buildings
    {
        [Description("Miniera di metallo")]
        MetalMine = 1,
        
        [Description("Miniera di cristallo")]
        CrystalMine = 2,

        [Description("Sintetizzatore di deuterio")]
        DeuteriumSynth = 3,

        [Description("Centrale solare")]
        SolarPlant = 4,

        [Description("Reattore nucleare")]
        FusionReactor = 12,

        [Description("Satellite Solare")]
        SolarSatellite = 212,

        [Description("Deposito di metallo")]
        MetalStore = 22,

        [Description("Deposito di cristallo")]
        CrystalStore = 23,

        [Description("Cisterna di deuterio")]
        DeuteriumTank = 24,

        [Description("Fabbrica dei robot")]
        RobotFactory = 14,
    }
}