﻿using System.ComponentModel;

namespace OGame.Browser
{
    /// <summary>
    /// Enum delle pagine di OGame
    /// </summary>
    enum Pages
    {
        [Description("overview")]
        Overview,

        [Description("resources")]
        Resources,

        [Description("station")]
        Station
    }
}