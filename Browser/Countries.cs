﻿using System.ComponentModel;
using System.Reflection;


namespace OGame.Browser
{
        /// <summary>
        /// Rappresenta una nazione
        /// </summary>
        enum Countries
        {
            [Description("http://ogame.it")]
            Italia,

            [Description("http://ae.ogame.org")]
            الامارات,

            [Description("http://ogame.com.ar")]
            Argentina,

            [Description("http://ogame.com.br")]
            Brazil,

            [Description("http://bg.ogame.org")]
            България,

            [Description("http://ogame.cz")]
            Česká_Republika,

            [Description("http://ogame.rs")]
            Србија,

            [Description("http://ogame.dk")]
            Danmark,

            [Description("http://ogame.de")]
            Deutschland,

            [Description("http://ogame.gr")]
            Ελλάδα,

            [Description("http://ogame.com.es")]
            España,

            [Description("http://ogame.fr")]
            France,

            [Description("http://ogame.com.hr")]
            Hrvatska,

            [Description("http://ogame.lv")]
            Latvija,

            [Description("http://ogame.lt")]
            Lietuva,

            [Description("http://ogame.hu")]
            Magyarország,

            [Description("http://mx.ogame.org")]
            México,

            [Description("http://ogame.nl")]
            Nederland,

            [Description("http://ogame.jp")]
            日本,

            [Description("http://ogame.no")]
            Norge,

            [Description("http://ogame.pl")]
            Polska,

            [Description("http://ogame.com.pt")]
            Portugal,

            [Description("http://ogame.ro")]
            Romania,

            [Description("http://ogame.ru")]
            Россия,

            [Description("http://ogame.si")]
            Slovenija,

            [Description("http://ogame.sk")]
            Slovensko,

            [Description("http://fi.ogame.org")]
            Suomi,

            [Description("http://ogame.se")]
            Sverige,

            [Description("http://ogame.tw")]
            台灣,

            [Description("http://tr.ogame.org")]
            Türkiye,

            [Description("http://ogame.org")]
            UK,

            [Description("http://ogame.us")]
            USA
        }
}