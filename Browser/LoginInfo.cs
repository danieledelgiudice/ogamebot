﻿
namespace OGame.Browser
{

    /// <summary>
    /// I dati necessari per il login
    /// </summary>
    struct LoginInfo
    {
        public readonly string id;
        public readonly string password;
        public readonly string server;
        public readonly Countries country;

        public LoginInfo(string id, string password, Countries country, string server)
        {
            this.id = id;
            this.password = password;
            this.country = country;
            this.server = server;
        }

        
    }
}