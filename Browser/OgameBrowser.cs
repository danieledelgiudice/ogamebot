﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using OGame.Building;
using OGame.Common;


namespace OGame.Browser
{
    /// <summary>
    /// Browser ottimizzato per compiere operazioni su OGame
    /// </summary>
    internal class OGameBrowser : WebBrowser
    {
        #region CanBuildReturnValues enum

        public enum CanBuildReturnValues
        {
            Yes,
            NotEnoughResources,
            InConstruction,
            Unavaiable
        }

        #endregion
        #region Building
        /// <summary>
        ///   Naviga fino alla pagina passata come parametro
        /// </summary>
        /// <param name = "page">La pagina verso cui vogliamo andare</param>
        /// <exception cref = "NotConnectedException">Solleva un eccezione se non si è connessi a internet</exception>
        private void BrowsePage(Pages page)
        {
            if (Document == null) throw new NotConnectedException();
            string url = Url.AbsoluteUri;
            var match = new Regex(@"(?<=page=)\w+").Match(url); //regex che ottiene la parola contenente il nome della pagina per sostituirlo con la nuova pagina
            Navigate(url.Replace(match.Value, page.GetDescription())); //es: da http://uni101.ogame.it/game/index.php?page=overview&etc ottiene overview

            var limit = DateTime.Now;       //soluzione inefficente al massimo, ma sono stato a cercare di risolverlo e questo è l'unico modo
            limit = limit.AddSeconds(2);
            while (DateTime.Now < limit)
            {
                Application.DoEvents();
            }

        }

        /// <summary>
        ///   Controlla se è possibile costruire un determinato edificio
        /// </summary>
        /// <param name = "b">L'edificio che si vuole controllare</param>
        /// <exception cref = "NotConnectedException">Solleva un eccezione se non si è connessi a internet</exception>
        /// <returns>Vero se è possibile crearlo, Falso se non è possibile</returns>
        public CanBuildReturnValues CanBuild(Buildings b)
        {
            BrowsePage(b.Page());
            if (Document.All.Count == 0) throw new NotConnectedException();

            foreach (HtmlElement element in Document.All)
            {
                if (element.GetAttribute("className") == "construction" && element.FirstChild != null && element.FirstChild.GetAttribute("className") == "pusher")
                    return CanBuildReturnValues.InConstruction;

                if (element.GetAttribute("ref") == "") continue;
                if (int.Parse(element.GetAttribute("ref")) != (int)b) continue;

                string @class = element.Parent.Parent.Parent.GetAttribute("className");
                switch (@class)
                {
                    case "on":
                        return CanBuildReturnValues.Yes;
                    case "off":
                        return CanBuildReturnValues.Unavaiable;
                    default:
                        return CanBuildReturnValues.NotEnoughResources;
                }
            }
            throw new NotConnectedException();
        }

        /// <summary>
        ///   Esegue la costruzione di un determinato edificio
        /// </summary>
        /// <param name = "b">L'edificio che si vuole costruire</param>
        /// <exception cref = "NotConnectedException">Solleva un eccezione se non si è connessi a internet</exception>
        public void Build(Buildings b)
        {

            BrowsePage(b.Page());

            if (Document == null) throw new NotConnectedException();
            foreach (HtmlElement element in Document.All)
            {
                if (element.GetAttribute("ref") == "") continue;
                if (int.Parse(element.GetAttribute("ref")) != (int)b) continue;
                element.Parent.FirstChild.InvokeMember("click");
                element.Parent.FirstChild.RaiseEvent("onclick");
                break;
            }
        }
        #endregion
        #region LoginUtilities
        /// <summary>
        ///   Effettua il login in OGame
        /// </summary>
        /// <param name = "info">Contiene le informazioni di accesso al gioco</param>
        /// <exception cref = "NotConnectedException">Solleva un eccezione se non si è connessi a internet</exception>
        /// <returns>Vero se l'accesso avviene con successo, altrimenti falso</returns>
        public bool Login(LoginInfo info)
        {
            Navigate(info.country.GetDescription());
            while (ReadyState != WebBrowserReadyState.Complete)
            {
                Application.DoEvents();
            }

            if (Document == null) throw new NotConnectedException();

            Document.GetElementById("loginBtn").InvokeMember("click");
            Document.GetElementById("serverLogin").SetAttribute("value", info.server);
            Document.GetElementById("serverLogin").RaiseEvent("onchange");
            Document.GetElementById("usernameLogin").SetAttribute("value", info.id);
            Document.GetElementById("passwordLogin").SetAttribute("value", info.password);
            Document.GetElementById("loginSubmit").InvokeMember("click");

            while (ReadyState != WebBrowserReadyState.Complete)
            {
                Application.DoEvents();
            }
            while (ReadyState != WebBrowserReadyState.Interactive)
            {
                Application.DoEvents();
            }


            string url = Document.Url.ToString();
            return !url.Contains("loginError");
        }


        

        /// <summary>
        ///   Ritorna la lista dei server di un determinato paese
        /// </summary>
        /// <param name = "country">Il paese di cui si vuole ottenere la lista</param>
        /// <exception cref = "NotConnectedException">Solleva un eccezione se non si è connessi a internet</exception>
        /// <returns>Coppia "Descrizione" -> "url"</returns>
        public static Dictionary<String, String> GetServerList(Countries country)
        {
            var dict = new Dictionary<String, String>();
            var wb = new WebBrowser();
            wb.Navigate(country.GetDescription());

            while (wb.ReadyState != WebBrowserReadyState.Complete)
            {
                Application.DoEvents();
            }

            if (wb.Document == null) throw new NotConnectedException();

            foreach (HtmlElement element in wb.Document.GetElementById("serverLogin").Children)
            {
                dict.Add(element.InnerText, element.GetAttribute("value"));
            }

            return dict;
        }

        /// <summary>
        ///   Disonnette l'utente dalla sessione e ritorna alla pagina iniziale
        /// </summary>
        /// <exception cref = "NotConnectedException">Solleva un eccezione se non si è connessi a internet</exception>
        public void Logout()
        {
            if (Document == null) throw new NotConnectedException();

            foreach (HtmlElement element in Document.All)
            {
                if (element.GetAttribute("href").Contains("logout"))
                {
                    element.InvokeMember("click");
                }
            }
        }

        #endregion
    }
}