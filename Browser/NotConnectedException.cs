﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace OGame.Browser
{
    [Serializable]
    public class NotConnectedException : Exception
    {
        public NotConnectedException() : base("Make sure that your connection is up.")
        {
        }
    }
}
