﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using OGame.Bot;
using OGame.Browser;

namespace OGame.Gui
{
    public partial class LoginWindow : Form
    {
        private OGameBot _bot;
        private UGInfos _UGInfo;

        public LoginWindow()
        {
            InitializeComponent();
        }
        #region Login
        private void DoLogIn()
        {
            loginButton.Enabled = false;
            _bot = new OGameBot();
            if (UGLogin())
                OGLogin();
            else
            {
                MessageBox.Show(null, @"Dati Unfair-Gamers inseriti non corretti", @"Autentificazione Fallita",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OGLogin()
        {
            _bot.LoginFailed +=
                o =>
                    {
                        MessageBox.Show(null, @"Dati OGame inseriti non corretti", @"Autentificazione Fallita",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                        if (loginButton.InvokeRequired)
                            loginButton.Invoke(new Action(() => loginButton.Enabled = true));
                    };
            _bot.LoginSuccessful +=
                delegate
                    {
                        if (InvokeRequired)
                        {
                            Invoke(new Action(delegate
                                                  {
                                                      Hide();
                                                      Form mainWindow = new MainWindow(this, _bot, _UGInfo);
                                                      mainWindow.Show();
                                                      loginButton.Enabled = true;
                                                      if (remeberCheckBox.Checked)
                                                      {
                                                          Properties.Settings.Default.OGid = OGUsernameTextBox.Text;
                                                          Properties.Settings.Default.OGpassword = OGPasswordTextBox.Text;
                                                          Properties.Settings.Default.OGcountry = OGCountryComboBox.SelectedIndex;
                                                          Properties.Settings.Default.OGserver = OGServerComboBox.SelectedIndex;
                                                          Properties.Settings.Default.UGid = UGusernameTextBox.Text;
                                                          Properties.Settings.Default.UGpassword = UGPasswordTextBox.Text;
                                                          Properties.Settings.Default.Save();
                                                      }
                                                  }));
                        }
                    };

            //fetching country
            Countries selectedCountry = (from field in typeof(Countries).GetFields(BindingFlags.Public | BindingFlags.Static)
                                         where Common.UsefulExtensions.GetDescription((Countries)field.GetValue(typeof(Countries))).Equals(OGCountryComboBox.SelectedValue)
                                         select (Countries)field.GetValue(typeof(Countries))
                        ).ToList()[0];
            _bot.Login(new LoginInfo(OGUsernameTextBox.Text, OGPasswordTextBox.Text, selectedCountry,
                                    (string)OGServerComboBox.SelectedValue));
        }

        private bool UGLogin()
        {
            //x D4.Ny -> sfrutta _bot.Browser per autentificare l'utente, poi riempi questa struct per le info da visualizzare nella seconda finestra
            _UGInfo = new UGInfos {User = "D4n13le", UID = "99999", Post = "320", Status = "Online"};
            return true;
        }

        #endregion

        #region Fetch Server
        private void PopulateCountryComboBox()
        {
            var list = (from field in typeof(Countries).GetFields(BindingFlags.Public | BindingFlags.Static)
                        select new
                        {
                            field.Name,
                            Url = Common.UsefulExtensions.GetDescription((Countries)field.GetValue(typeof(Countries)))
                        }).ToList();

            OGCountryComboBox.DataSource = list;

            OGCountryComboBox.DisplayMember = "Name";
            OGCountryComboBox.ValueMember = "Url";
        }

        private void PopulateServerComboBox(Countries country)
        {
            OGServerComboBox.Enabled = false;
            var list = (from pair in OGameBrowser.GetServerList(country)
                        select new
                        {
                            Name = pair.Key,
                            Url = pair.Value
                        }).ToList();

            OGServerComboBox.DataSource = list;
            OGServerComboBox.DisplayMember = "Name";
            OGServerComboBox.ValueMember = "Url";
            OGServerComboBox.Enabled = true;
        }
        #endregion
        #region Events

        private void OnLoad(object sender, EventArgs e)
        {
            PopulateCountryComboBox();

            OGUsernameTextBox.Text = Properties.Settings.Default.OGid;
            OGPasswordTextBox.Text = Properties.Settings.Default.OGpassword;
            OGCountryComboBox.SelectedIndex = Properties.Settings.Default.OGcountry;
            OGServerComboBox.SelectedIndex = Properties.Settings.Default.OGserver;
            UGusernameTextBox.Text = Properties.Settings.Default.UGid;
            UGPasswordTextBox.Text = Properties.Settings.Default.UGpassword;
            remeberCheckBox.Checked = Properties.Settings.Default.rememberCheckBoxState;
        }

        private void LoginFieldsController(object sender, EventArgs e)
        {
            loginButton.Enabled = UGPasswordTextBox.TextLength > 0 &&
                                  UGusernameTextBox.TextLength > 0 &&
                                  OGPasswordTextBox.TextLength > 0 &&
                                  OGUsernameTextBox.TextLength > 0;


        }

        private void OnOGCountryComboBoxIndexChanged(object sender, EventArgs e)
        {
            PopulateServerComboBox((Countries)((ComboBox)sender).SelectedIndex);
        }

        private void OnRegisterLabelClick(object sender, EventArgs e)
        {
            new System.Diagnostics.Process
            {
                StartInfo =
                {
                    FileName = @"http://www.unfair-gamers.com/forum/member.php?action=register",
                    CreateNoWindow = false
                }
            }.Start();
        }

        private void LoginButtonClick(object sender, EventArgs e)
        {
            DoLogIn();
        }

        private void remeberCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = ((CheckBox) sender).Checked;
            if (!isChecked)
            {
                Properties.Settings.Default.OGid = "";
                Properties.Settings.Default.OGpassword = "";
                Properties.Settings.Default.OGcountry = 0;
                Properties.Settings.Default.OGserver = 0;
                Properties.Settings.Default.UGid = "";
                Properties.Settings.Default.UGpassword = "";
            }
            Properties.Settings.Default.rememberCheckBoxState = isChecked;
            Properties.Settings.Default.Save();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            profileLabel.Visible = !profileLabel.Visible;
            GuidInfoLabel.Visible = !GuidInfoLabel.Visible;
        }

        private void profileLabel_Click(object sender, EventArgs e)
        {
            new System.Diagnostics.Process
            {
                StartInfo =
                {
                    FileName = @"http://www.unfair-gamers.com/forum/usercp.php?action=profile",
                    CreateNoWindow = false
                }
            }.Start();
        }

        private void guidButton_Click(object sender, EventArgs e)
        {
            //x D4.Ny -> metti qua il codice per calcolare il guid e per metterlo nella guidTextBox
        }
#endregion

        
    }
}
