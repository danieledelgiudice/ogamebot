﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using OGame.Bot;
using OGame.Building;
using OGame.Common;

namespace OGame.Gui
{
    partial class MainWindow : Form
    {
        private readonly Form _parent;
        private readonly OGameBot _bot;
        private bool _started;

        private int _notEnoughResourcesCount; //per non mostrare ogni 5 secondi che non ci sono risorse
        private int _inConstructionCount;

        public MainWindow(Form parent, OGameBot bot, UGInfos forumInfos)
        {
            _bot = bot;
            _parent = parent;
            InitializeComponent();
            forumUserLabel.Text = @"User: " + forumInfos.User;
            forumUIDLabel.Text = @"Post: " + forumInfos.Post;
            forumPostLabel.Text = @"UID: " + forumInfos.UID;
            forumStatusLabel.Text = @"Status: " + forumInfos.Status;
            PopulateAddBuildingList();

            _bot.BeganBuild += (o, b) => DisplayLogMessage(@"Iniziata la costruzione di " + b.GetDescription());

            _bot.Built += (o, b) =>
                              {
                                  DisplayLogMessage(@"Terminata la costruzione di " + b.GetDescription());
                                  buildingListView.Invoke(new Action(() => buildingListView.Items.RemoveAt(0)));
                              };

            _bot.BuildingUnavaiable += (o, b) => DisplayLogMessage(@"Non puoi costruire " + b.GetDescription() + ", verrà spostato in fondo alla coda.");
            _bot.NotEnoughResources += (o, b) =>
                                           {
                                               if (_notEnoughResourcesCount == 0)
                                                   DisplayLogMessage(@"Risorse non sufficenti per " + b.GetDescription() +
                                                                     ", in attesa");
                                               _notEnoughResourcesCount = ++_notEnoughResourcesCount%20;
                                           };

            _bot.InConstruction += o =>
                                       {
                                           if (_inConstructionCount == 0)
                                               DisplayLogMessage(
                                                   @"E' già presente un edificio in costruzione, in attesa");
                                           _inConstructionCount = ++_inConstructionCount%20;
                                       };
            _bot.Stopped += o => _parent.Invoke(new Action(Close));

        }

        private void DisplayLogMessage(string message)
        {
            if (logTextBox.InvokeRequired)
                logTextBox.Invoke(new Action(() => logTextBox.Text += String.Format("{0:d/M/yy HH:mm:ss} - {1}\r\n", DateTime.Now, message)));
        }

        private void StartStopButtonClick(object sender, EventArgs e)
        {
            if (!_started)
            {
                if (buildingListView.Items.Count == 0)
                {
                    MessageBox.Show(this, @"Devi inserire almeno un elemento nella coda di costruzione", @"Errore!",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                _bot.Start();
                ((Button)sender).Text = @"Ferma";
                _started = true;
            } else
            {
                Close();
            }
        }

        private void MainWindowFormClosed(object sender, FormClosedEventArgs e)
        {
            _bot.Terminate();
            _parent.Close();
        }
        #region ListViewManagement
        private void MoveUpBuildingClick(object sender, EventArgs e)
        {
            //LISTVIEW SECTION

            if (buildingListView.SelectedItems.Count == 0)
                return;

            int selIdx = buildingListView.SelectedIndices[0];

            if (selIdx == 0)
                return;

            foreach (ListViewItem item in buildingListView.SelectedItems)
            {
                item.Selected = false;
            }

            for (int i = 0; i < buildingListView.Items[selIdx].SubItems.Count; i++)
            {
                string cache = buildingListView.Items[selIdx - 1].SubItems[i].Text;
                buildingListView.Items[selIdx - 1].SubItems[i].Text =
                buildingListView.Items[selIdx].SubItems[i].Text;
                buildingListView.Items[selIdx].SubItems[i].Text = cache;
            }
            buildingListView.Items[selIdx - 1].Selected = true;
            buildingListView.Refresh();
            buildingListView.Focus();

            //BUILDINGLIST SECTION
            Buildings tmp = _bot.BuildingList[selIdx];
            _bot.BuildingList[selIdx] = _bot.BuildingList[selIdx - 1];
            _bot.BuildingList[selIdx - 1] = tmp;

        }

        private void MoveDownBuildingClick(object sender, EventArgs e)
        {
            //LISTVIEW SECTION

            if (buildingListView.SelectedItems.Count == 0)
                return;

            int selIdx = buildingListView.SelectedIndices[0];

            if (selIdx == buildingListView.Items.Count - 1)
                return;

            foreach (ListViewItem item in buildingListView.SelectedItems)
            {
                item.Selected = false;
            }

            for (int i = 0; i < buildingListView.Items[selIdx].SubItems.Count; i++)
            {
                string cache = buildingListView.Items[selIdx + 1].SubItems[i].Text;
                buildingListView.Items[selIdx + 1].SubItems[i].Text =
                buildingListView.Items[selIdx].SubItems[i].Text;
                buildingListView.Items[selIdx].SubItems[i].Text = cache;
            }
            buildingListView.Items[selIdx + 1].Selected = true;
            buildingListView.Refresh();
            buildingListView.Focus();

            //BUILDINGLIST SECTION
            Buildings tmp = _bot.BuildingList[selIdx];
            _bot.BuildingList[selIdx] = _bot.BuildingList[selIdx + 1];
            _bot.BuildingList[selIdx + 1] = tmp;
        }

        private void RemoveBuildingClick(object sender, EventArgs e)
        {
            while (buildingListView.SelectedIndices.Count > 0)
            {
                _bot.BuildingList.RemoveAt(buildingListView.SelectedIndices[0]);
                buildingListView.Items.RemoveAt(buildingListView.SelectedIndices[0]);
            }
        }
        
        private void ListViewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    RemoveBuildingClick(sender, e);
                    break;
                case Keys.Up:
                    MoveUpBuildingClick(sender, e);
                    break;
                case Keys.Down:
                    MoveDownBuildingClick(sender, e);
                    break;
            }
        }


        private void PopulateAddBuildingList()
        {
            var list = (from field in typeof (Buildings).GetFields(BindingFlags.Public | BindingFlags.Static)
                        select new ToolStripMenuItem().With( w =>
                                                                 {
                                       w.Text = ((Buildings)field.GetValue(typeof (Buildings))).GetDescription();
                                       w.Tag = (int)field.GetValue(typeof (Buildings));
                                       w.Click += delegate(object sender, EventArgs e)
                                            {
                                                buildingListView.Items.Add(((ToolStripMenuItem)sender).Text);
                                                _bot.BuildingList.Add(
                                                    (Buildings) ((ToolStripMenuItem) sender).Tag);
                                            };
                                       })).ToArray();

            toolStripSplitButton1.DropDownItems.AddRange(new ToolStripItemCollection(toolStrip1, list));
        }
        #endregion
    }
}
