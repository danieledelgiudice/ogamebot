﻿namespace OGame.Gui
{
    partial class LoginWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginWindow));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.remeberCheckBox = new System.Windows.Forms.CheckBox();
            this.loginButton = new System.Windows.Forms.Button();
            this.OGameLoginGroupBox = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.OGServerComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.OGCountryComboBox = new System.Windows.Forms.ComboBox();
            this.OGPasswordTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.OGUsernameTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.UGLoginGroupBox = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.guidButton = new System.Windows.Forms.Button();
            this.guidTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.UGPasswordTextBox = new System.Windows.Forms.TextBox();
            this.UGusernameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GuidInfoLabel = new System.Windows.Forms.Label();
            this.profileLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.OGameLoginGroupBox.SuspendLayout();
            this.UGLoginGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(129, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(358, 94);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::OGame.Properties.Resources.stripes;
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Location = new System.Drawing.Point(-3, -1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(603, 118);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(-3, 116);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(606, 5);
            this.panel4.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(224)))), ((int)(((byte)(238)))));
            this.panel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel5.BackgroundImage")));
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel5.Location = new System.Drawing.Point(0, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(605, 320);
            this.panel5.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::OGame.Properties.Resources.bg_clouds;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(-1, 119);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(619, 338);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.remeberCheckBox);
            this.panel2.Controls.Add(this.loginButton);
            this.panel2.Controls.Add(this.OGameLoginGroupBox);
            this.panel2.Controls.Add(this.UGLoginGroupBox);
            this.panel2.Location = new System.Drawing.Point(21, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(552, 284);
            this.panel2.TabIndex = 0;
            // 
            // remeberCheckBox
            // 
            this.remeberCheckBox.AutoSize = true;
            this.remeberCheckBox.Location = new System.Drawing.Point(450, 253);
            this.remeberCheckBox.Name = "remeberCheckBox";
            this.remeberCheckBox.Size = new System.Drawing.Size(94, 17);
            this.remeberCheckBox.TabIndex = 0;
            this.remeberCheckBox.Text = "Remember me";
            this.remeberCheckBox.UseVisualStyleBackColor = true;
            this.remeberCheckBox.CheckedChanged += new System.EventHandler(this.remeberCheckBox_CheckedChanged);
            // 
            // loginButton
            // 
            this.loginButton.Enabled = false;
            this.loginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginButton.Location = new System.Drawing.Point(293, 183);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(251, 60);
            this.loginButton.TabIndex = 7;
            this.loginButton.Text = "LOG IN!";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.LoginButtonClick);
            // 
            // OGameLoginGroupBox
            // 
            this.OGameLoginGroupBox.Controls.Add(this.label11);
            this.OGameLoginGroupBox.Controls.Add(this.OGServerComboBox);
            this.OGameLoginGroupBox.Controls.Add(this.label10);
            this.OGameLoginGroupBox.Controls.Add(this.OGCountryComboBox);
            this.OGameLoginGroupBox.Controls.Add(this.OGPasswordTextBox);
            this.OGameLoginGroupBox.Controls.Add(this.label9);
            this.OGameLoginGroupBox.Controls.Add(this.OGUsernameTextBox);
            this.OGameLoginGroupBox.Controls.Add(this.label8);
            this.OGameLoginGroupBox.Location = new System.Drawing.Point(293, 6);
            this.OGameLoginGroupBox.Name = "OGameLoginGroupBox";
            this.OGameLoginGroupBox.Size = new System.Drawing.Size(251, 171);
            this.OGameLoginGroupBox.TabIndex = 1;
            this.OGameLoginGroupBox.TabStop = false;
            this.OGameLoginGroupBox.Text = "OGame";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Server";
            // 
            // OGServerComboBox
            // 
            this.OGServerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OGServerComboBox.FormattingEnabled = true;
            this.OGServerComboBox.Location = new System.Drawing.Point(83, 129);
            this.OGServerComboBox.Name = "OGServerComboBox";
            this.OGServerComboBox.Size = new System.Drawing.Size(139, 21);
            this.OGServerComboBox.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Country";
            // 
            // OGCountryComboBox
            // 
            this.OGCountryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OGCountryComboBox.FormattingEnabled = true;
            this.OGCountryComboBox.Location = new System.Drawing.Point(83, 91);
            this.OGCountryComboBox.Name = "OGCountryComboBox";
            this.OGCountryComboBox.Size = new System.Drawing.Size(139, 21);
            this.OGCountryComboBox.TabIndex = 5;
            this.OGCountryComboBox.SelectedIndexChanged += new System.EventHandler(this.OnOGCountryComboBoxIndexChanged);
            // 
            // OGPasswordTextBox
            // 
            this.OGPasswordTextBox.Location = new System.Drawing.Point(83, 56);
            this.OGPasswordTextBox.Name = "OGPasswordTextBox";
            this.OGPasswordTextBox.Size = new System.Drawing.Size(139, 20);
            this.OGPasswordTextBox.TabIndex = 4;
            this.OGPasswordTextBox.UseSystemPasswordChar = true;
            this.OGPasswordTextBox.TextChanged += new System.EventHandler(this.LoginFieldsController);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Password";
            // 
            // OGUsernameTextBox
            // 
            this.OGUsernameTextBox.Location = new System.Drawing.Point(83, 20);
            this.OGUsernameTextBox.Name = "OGUsernameTextBox";
            this.OGUsernameTextBox.Size = new System.Drawing.Size(139, 20);
            this.OGUsernameTextBox.TabIndex = 3;
            this.OGUsernameTextBox.TextChanged += new System.EventHandler(this.LoginFieldsController);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Username";
            // 
            // UGLoginGroupBox
            // 
            this.UGLoginGroupBox.BackColor = System.Drawing.Color.White;
            this.UGLoginGroupBox.Controls.Add(this.profileLabel);
            this.UGLoginGroupBox.Controls.Add(this.GuidInfoLabel);
            this.UGLoginGroupBox.Controls.Add(this.label7);
            this.UGLoginGroupBox.Controls.Add(this.label6);
            this.UGLoginGroupBox.Controls.Add(this.label1);
            this.UGLoginGroupBox.Controls.Add(this.label5);
            this.UGLoginGroupBox.Controls.Add(this.guidButton);
            this.UGLoginGroupBox.Controls.Add(this.guidTextBox);
            this.UGLoginGroupBox.Controls.Add(this.label4);
            this.UGLoginGroupBox.Controls.Add(this.panel6);
            this.UGLoginGroupBox.Controls.Add(this.UGPasswordTextBox);
            this.UGLoginGroupBox.Controls.Add(this.UGusernameTextBox);
            this.UGLoginGroupBox.Controls.Add(this.label3);
            this.UGLoginGroupBox.Controls.Add(this.label2);
            this.UGLoginGroupBox.Location = new System.Drawing.Point(10, 6);
            this.UGLoginGroupBox.Name = "UGLoginGroupBox";
            this.UGLoginGroupBox.Size = new System.Drawing.Size(277, 264);
            this.UGLoginGroupBox.TabIndex = 0;
            this.UGLoginGroupBox.TabStop = false;
            this.UGLoginGroupBox.Text = "Unfair-Gamers";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(198, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "or login with your Unfair-Gamers account";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label6.Location = new System.Drawing.Point(30, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Register";
            this.label6.Click += new System.EventHandler(this.OnRegisterLabelClick);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(11, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 16);
            this.label1.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label5.Location = new System.Drawing.Point(92, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Where should I put the GUID?";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // guidButton
            // 
            this.guidButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.guidButton.Location = new System.Drawing.Point(10, 135);
            this.guidButton.Name = "guidButton";
            this.guidButton.Size = new System.Drawing.Size(77, 20);
            this.guidButton.TabIndex = 8;
            this.guidButton.TabStop = false;
            this.guidButton.Text = "Get my GUID";
            this.guidButton.UseVisualStyleBackColor = true;
            this.guidButton.Click += new System.EventHandler(this.guidButton_Click);
            // 
            // guidTextBox
            // 
            this.guidTextBox.Location = new System.Drawing.Point(98, 135);
            this.guidTextBox.Name = "guidTextBox";
            this.guidTextBox.Size = new System.Drawing.Size(138, 20);
            this.guidTextBox.TabIndex = 7;
            this.guidTextBox.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 6;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel6.Location = new System.Drawing.Point(10, 119);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(253, 1);
            this.panel6.TabIndex = 5;
            // 
            // UGPasswordTextBox
            // 
            this.UGPasswordTextBox.Location = new System.Drawing.Point(98, 84);
            this.UGPasswordTextBox.Name = "UGPasswordTextBox";
            this.UGPasswordTextBox.Size = new System.Drawing.Size(138, 20);
            this.UGPasswordTextBox.TabIndex = 2;
            this.UGPasswordTextBox.UseSystemPasswordChar = true;
            this.UGPasswordTextBox.TextChanged += new System.EventHandler(this.LoginFieldsController);
            // 
            // UGusernameTextBox
            // 
            this.UGusernameTextBox.Location = new System.Drawing.Point(98, 51);
            this.UGusernameTextBox.Name = "UGusernameTextBox";
            this.UGusernameTextBox.Size = new System.Drawing.Size(138, 20);
            this.UGusernameTextBox.TabIndex = 1;
            this.UGusernameTextBox.TextChanged += new System.EventHandler(this.LoginFieldsController);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Username";
            // 
            // GuidInfoLabel
            // 
            this.GuidInfoLabel.Location = new System.Drawing.Point(10, 187);
            this.GuidInfoLabel.Name = "GuidInfoLabel";
            this.GuidInfoLabel.Size = new System.Drawing.Size(261, 60);
            this.GuidInfoLabel.TabIndex = 13;
            this.GuidInfoLabel.Text = "Go on your            , and put it in the field \"GUID\" under \"Informazioni aggiun" +
                "tive\".Then press the button \"Aggiorna il profilo\" on the bottom side of page.";
            this.GuidInfoLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.GuidInfoLabel.Visible = false;
            // 
            // profileLabel
            // 
            this.profileLabel.BackColor = System.Drawing.Color.Transparent;
            this.profileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileLabel.ForeColor = System.Drawing.SystemColors.Highlight;
            this.profileLabel.Location = new System.Drawing.Point(78, 187);
            this.profileLabel.Margin = new System.Windows.Forms.Padding(0);
            this.profileLabel.Name = "profileLabel";
            this.profileLabel.Size = new System.Drawing.Size(35, 15);
            this.profileLabel.TabIndex = 14;
            this.profileLabel.Text = "profile";
            this.profileLabel.Visible = false;
            this.profileLabel.Click += new System.EventHandler(this.profileLabel_Click);
            // 
            // LoginWindow
            // 
            this.AcceptButton = this.loginButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(224)))), ((int)(((byte)(238)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(593, 436);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LoginWindow";
            this.Text = "UG-OGameBot - Login";
            this.Load += new System.EventHandler(this.OnLoad);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.OGameLoginGroupBox.ResumeLayout(false);
            this.OGameLoginGroupBox.PerformLayout();
            this.UGLoginGroupBox.ResumeLayout(false);
            this.UGLoginGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.GroupBox OGameLoginGroupBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox OGServerComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox OGCountryComboBox;
        private System.Windows.Forms.TextBox OGPasswordTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox OGUsernameTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox UGLoginGroupBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button guidButton;
        private System.Windows.Forms.TextBox guidTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox UGPasswordTextBox;
        private System.Windows.Forms.TextBox UGusernameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox remeberCheckBox;
        private System.Windows.Forms.Label profileLabel;
        private System.Windows.Forms.Label GuidInfoLabel;
    }
}