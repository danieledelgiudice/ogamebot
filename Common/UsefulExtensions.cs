﻿using System.Reflection;
using System.ComponentModel;
using System;
using OGame.Browser;

namespace OGame.Common
{

    static class UsefulExtensions
    {
        public static string GetDescription(this Enum country)
        {
            FieldInfo fi = country.GetType().GetField(country.ToString());

            var attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            return attributes.Length > 0 ? attributes[0].Description : country.ToString();
        }

        public static Pages Page(this Building.Buildings building)
        {
            int @ref = (int) building;
            return @ref <= 12 || (@ref >= 22 && @ref <= 24) || @ref == 212 ? Pages.Resources : Pages.Station;
        }

        public static T With<T>(this T with, Action<T> action) where T : class
        {
            if (with != null)
                action(with);
            return with;
        }
    }
}