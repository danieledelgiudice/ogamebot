﻿using System;

namespace OGame.Bot
{
    class NotLoggedException : Exception
    {
        public NotLoggedException()
            : base("You must login before!")
        {
        }
    }
}
