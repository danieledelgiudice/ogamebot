﻿using System;
using OGame.Browser;
using System.Threading;
using OGame.Building;

namespace OGame.Bot
{
    class OGameBot
    {
        private bool _loggedIn;
        private bool _started;
        private LoginInfo _loginInfo;

        #region Bot Events
        public event Action<object> Stopped;
        public event Action<object> LoginSuccessful;
        public event Action<object> LoginFailed;
        public event Action<object, Buildings> Built;
        public event Action<object, Buildings> BeganBuild;
        public event Action<object, Buildings> NotEnoughResources;
        public event Action<object, Buildings> BuildingUnavaiable;
        public event Action<object> InConstruction;
        #endregion
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public OGameBot()
        {
            BuildingList = new BuildingList();
        }

        /// <summary>
        /// Lista degli edifici in costruzione
        /// </summary>
        public BuildingList BuildingList { get; private set; }

        /// <summary>
        /// Istanza dell'OGameBrowser
        /// </summary>
        public OGameBrowser Browser { get; private set; }

        /// <summary>
        /// Termina l'esecuzione del bot
        /// </summary>
        public void Terminate()
        {
            _started = true;
        }

        /// <summary>
        /// Si autentifica all'interno del gioco
        /// </summary>
        /// <param name="info">Struttura contenente le informazioni necessarie per il login.</param>
        /// <returns>True se il bot riesce ad autentificarsi correttamente, altrimenti False</returns>
        public void Login(LoginInfo info)
        {
            _loginInfo = info;
            var thr = new Thread(BotLoop) { IsBackground = true };
            thr.SetApartmentState(ApartmentState.STA);
            thr.Start();
        }

        /// <summary>
        /// Dà il via all'esecuzione del bot
        /// </summary>
        public void Start()
        {
            if (!_loggedIn) throw new NotLoggedException();
            _started = true;
            
        }

        /// <summary>
        /// Loop che del bot
        /// </summary>
        private void BotLoop()
        {
            Browser = new OGameBrowser();

            if (!Browser.Login(_loginInfo)) //login in ogame
            {
                if (LoginFailed != null) LoginFailed(this);
                return;
            }
            if (LoginSuccessful != null) LoginSuccessful(this);
            _loggedIn = true;

            while (!_started) //aspettando l'attivazione del bot
            {
                Thread.Sleep(3000);
            }

            while (_started && BuildingList.Count > 0) //controlla che non sia stato stoppato manualmente e che non siano terminati gli elementi
            {
                OGameBrowser.CanBuildReturnValues canBuild = Browser.CanBuild(BuildingList[0]);
                switch(canBuild)
                {
                    case OGameBrowser.CanBuildReturnValues.Unavaiable:
                        if (BuildingUnavaiable != null) BuildingUnavaiable(this, BuildingList[0]);
                        BuildingList.RemoveAt(0); //se non è costruibile lo rimuovo dalla lista e lancio l'evento
                        break;

                    case OGameBrowser.CanBuildReturnValues.NotEnoughResources:
                        if (NotEnoughResources != null) NotEnoughResources(this, BuildingList[0]); //se non ci sono risorse lancio l'evento e non faccio altro
                        break;

                    case OGameBrowser.CanBuildReturnValues.Yes:
                        Browser.Build(BuildingList[0]); //se posso costruire costruisco e lancio l'evento
                        if (BeganBuild != null) BeganBuild(this, BuildingList[0]);
                        while (Browser.CanBuild(BuildingList[0]) == OGameBrowser.CanBuildReturnValues.InConstruction) //ciclo che aspetta fin quando smette di esserci un edificio in costruzione
                        {
                            if (!_started) goto Stop;
                            Thread.Sleep(5000);
                        }
                            
                        if (Built != null) Built(this, BuildingList[0]); //lancio l'evento a costruzione terminata e rimuovo l'edificio dalla lista
                        BuildingList.RemoveAt(0);
                        break;

                    case OGameBrowser.CanBuildReturnValues.InConstruction:
                        if (InConstruction != null) InConstruction(this); //se c'è già un edificio in costruzione (partito prima dell'avvio del bot), lancio l'evento e aspetto
                        break;
                }
                Thread.Sleep(5000);
            }
            Stop:
            if (Stopped != null) Stopped(this); //a questo punto esco
        }
    }
}
